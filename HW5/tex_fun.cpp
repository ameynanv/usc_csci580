/* Texture functions for cs580 GzLib	*/
#include	<stdlib.h>
#include	<stdio.h>
#include	<math.h>
#include	"gz.h"

GzColor	*image;
int xs, ys;
int reset = 1;

void getTextureFromImage(GzColor *image, float u, float v, GzColor color) {
	if (u < 0) u = 0;
	if (v < 0) v = 0;
	if (u > 1) u = 1;
	if (v > 1) v = 1;

	// translate u,v coordinates to texture pixels
    // find the bounding box
	int maxU = ceil(u * (xs - 1));
	int minU = floor(u * (xs - 1));
	int maxV = ceil(v * (ys - 1));
	int minV = floor(v * (ys - 1));

	// find the colors at the four corners
	GzColor colA,colB,colC,colD;
	colA[RED] = image[minU + (minV * xs)][RED];
	colB[RED] = image[minU + (maxV * xs)][RED];
	colC[RED] = image[maxU + (maxV * xs)][RED];
	colD[RED] = image[maxU + (minV * xs)][RED];

	colA[GREEN] = image[minU + (minV * xs)][GREEN];
	colB[GREEN] = image[minU + (maxV * xs)][GREEN];
	colC[GREEN] = image[maxU + (maxV * xs)][GREEN];
	colD[GREEN] = image[maxU + (minV * xs)][GREEN];

	colA[BLUE] = image[minU + (minV * xs)][BLUE];
	colB[BLUE] = image[minU + (maxV * xs)][BLUE];
	colC[BLUE] = image[maxU + (maxV * xs)][BLUE];
	colD[BLUE] = image[maxU + (minV * xs)][BLUE];

	// find the values of s, t
	float s = u * (xs - 1) - minU;
	float t = v * (ys - 1) - minV;

	// use bilinear interpolation to find the color
	color[RED] = (1-s) * (1-t) * colA[RED] + (1-s) * t * colB[RED] + s * t * colC[RED] + s * (1 - t) * colD[RED];
	color[GREEN] = (1-s) * (1-t) * colA[GREEN] + (1-s) * t * colB[GREEN] + s * t * colC[GREEN] + s * (1 - t) * colD[GREEN];
	color[BLUE] = (1-s) * (1-t) * colA[BLUE] + (1-s) * t * colB[BLUE] + s * t * colC[BLUE] + s * (1 - t) * colD[BLUE];
}

/* Image texture function */
int tex_fun(float u, float v, GzColor color)
{
	unsigned char	pixel[3];
	unsigned char	dummy;
	char		foo[8];
	int		i;
	FILE		*fd;

	if (reset) {          /* open and load texture file */
		fd = fopen ("texture", "rb");
		if (fd == NULL) {
			fprintf (stderr, "texture file not found\n");
			return GZ_FAILURE;
		}
		fscanf (fd, "%s %d %d %c", foo, &xs, &ys, &dummy);
		image = (GzColor*)malloc(sizeof(GzColor)*(xs+1)*(ys+1));
		if (image == NULL) {
			fprintf (stderr, "malloc for texture image failed\n");
			return GZ_FAILURE;
		}

		for (i = 0; i < xs*ys; i++) {	/* create array of GzColor values */
			fread(pixel, sizeof(pixel), 1, fd);
			image[i][RED] = (float)((int)pixel[RED]) * (1.0 / 255.0);
			image[i][GREEN] = (float)((int)pixel[GREEN]) * (1.0 / 255.0);
			image[i][BLUE] = (float)((int)pixel[BLUE]) * (1.0 / 255.0);
		}

		reset = 0;          /* init is done */
		fclose(fd);
	}

	/* bounds-test u,v to make sure nothing will overflow image array bounds */
	/* determine texture cell corner values and perform bilinear interpolation */
	/* set color to interpolated GzColor value and return */
	getTextureFromImage(image, u,v, color);
    return GZ_SUCCESS;
}

/* Procedural texture function */
int ptex_fun(float u, float v, GzColor color)
{
	if (reset) {
		xs = 1000;
		ys = 1000;
		image = (GzColor*)malloc(sizeof(GzColor)*(xs+1)*(ys+1));
		for (int x = 0 ; x < xs; x++) {
			for (int y = 0; y < ys; y++) {
				image[x + y * xs][RED] = 0.0; image[x + y * xs][GREEN] = 0.0; image[x + y * xs][BLUE] = 1.0;
			}
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				int x_origin = i * (xs/10) + 50; int y_origin = j * (ys/10) + 50;
				for (int r = 1; r < 30; r=r+1) {
					for (float theta = 0.0; theta < 2 * M_PI; theta = theta + 0.001) {
						int x = (r * cos(theta) + x_origin);
						int y = (r * sin(theta) + y_origin);
						if (x > 0 && x < xs && y > 0 && y < ys) {
							image[x + y * xs][RED] = 1.0; image[x + y * xs][GREEN] = 0.0; image[x + y * xs][BLUE] = 0.0;
						}
					}
				}
			}
		}
		reset = 0;
	}
	getTextureFromImage(image, u,v, color);
	return GZ_SUCCESS;
}

