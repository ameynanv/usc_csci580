/**
 * Main entry point
 * USC csci 580 *
*/

#include "Application5.h"

int main(int argc, const char * argv[])
{
  Application6 application;

  application.runRenderer();
  
  return 0;
}

