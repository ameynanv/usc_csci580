/* 
*  disp.cpp -- definition file for Display
*  USC csci 580 
*/

#include "gz.h"
#include "disp.h"
#include <stdlib.h>

int GzNewFrameBuffer(char** framebuffer, int width, int height)
{
  /* create a framebuffer:
   -- allocate memory for framebuffer : (sizeof)GzPixel x width x height
   -- pass back pointer
  */
  *framebuffer = (char *) malloc(sizeof(GzPixel) * width * height);
  return GZ_SUCCESS;
}

int GzNewDisplay(GzDisplay **display, GzDisplayClass dispClass, int xRes, int yRes)
{
  /* create a display:
   -- allocate memory for indicated class and resolution
   -- pass back pointer to GzDisplay object in display
   */
  *display = (GzDisplay *) malloc(sizeof(GzDisplay));
  (*display)->xres = xRes;
  (*display)->yres = yRes;
  (*display)->dispClass = dispClass;
  (*display)->fbuf = (GzPixel *)malloc(sizeof(GzPixel) * xRes * yRes);
  return GZ_SUCCESS;
}


int GzFreeDisplay(GzDisplay *display)
{
  /* clean up, free memory */
  free(display);
  return GZ_SUCCESS;
}


int GzGetDisplayParams(GzDisplay *display, int *xRes, int *yRes, GzDisplayClass	*dispClass)
{
  /* pass back values for an open display */
  *xRes = display->xres;
  *yRes = display->yres;
  *dispClass = display->dispClass;
  return GZ_SUCCESS;
}


int GzInitDisplay(GzDisplay *display)
{
  /* set everything to some default values - start a new frame */
  for (int x = 0 ; x < display->xres; x++) {
	  for (int y = 0; y < display->yres; y++) {
		  GzPixel pix;
		  pix.red = 2048;
		  pix.green = 1792;
		  pix.blue = 1536;
		  pix.alpha = 1;
		  pix.z = 0;
		  display->fbuf[x * display->xres +y] = pix;
	  }
  }
  return GZ_SUCCESS;
}


int GzPutDisplay(GzDisplay *display, int i, int j, GzIntensity r, GzIntensity g, GzIntensity b, GzIntensity a, GzDepth z)
{
  /* write pixel values into the display */
  if (i <= display->xres && i >= 0 && j <= display->yres && j >= 0) {
	  GzPixel pix;
	  pix.red = r;
	  pix.green = g;
	  pix.blue = b;
	  pix.alpha = a;
	  pix.z = z;
	  display->fbuf[j * display->xres + i] = pix;
  }
  return GZ_SUCCESS;
}


int GzGetDisplay(GzDisplay *display, int i, int j, GzIntensity *r, GzIntensity *g, GzIntensity *b, GzIntensity *a, GzDepth *z)
{
  /* pass back pixel value in the display */
  /* check display class to see what vars are valid */

  if (i <= display->xres && i >= 0 && j <= display->yres && j >= 0) {
	  *r = display->fbuf[j * display->xres + i].red;
	  *g = display->fbuf[j * display->xres + i].green;
	  *b = display->fbuf[j * display->xres + i].blue;
	  *a = display->fbuf[j * display->xres + i].alpha;
	  *z = display->fbuf[j * display->xres + i].z;
  } else {
	  *r = 2048;
	  *g = 1792;
	  *b = 1536;
	  *a = 1;
	  *z = 0;
  }

  return GZ_SUCCESS;
}


int GzFlushDisplay2File(FILE* outfile, GzDisplay *display)
{
  /* write pixels to ppm file based on display class -- "P6 %d %d 255\r" */
  fprintf(outfile, "P3 %d %d 4095 \r", display->xres, display->yres);
  for (int x = 0 ; x < display->xres; x++) {
  	  for (int y = 0; y < display->yres; y++) {
  		  fprintf(outfile, "%d %d %d ", display->fbuf[x * display->xres +y].red, display->fbuf[x * display->xres +y].green, display->fbuf[x * display->xres +y].blue);
  	  }
  	  fprintf(outfile, "\r");
    }
  return GZ_SUCCESS;
}

int GzFlushDisplay2FrameBuffer(char* framebuffer, GzDisplay *display)
{
  /* write pixels to framebuffer:
   - Put the pixels into the frame buffer
   - Caution: store the pixel to the frame buffer as the order of blue, green, and red
   - Not red, green, and blue !!!
  */
  
  return GZ_SUCCESS;
}
