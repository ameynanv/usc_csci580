#include	"gz.h"
#include	"rend.h"
#include    "disp.h"
#include 	<math.h>
#include 	<stdlib.h>
#include 	<iostream>
#include 	<stdio.h>

struct Vertex {
	float x,y,z;
};

struct Edge {
	float x1,y1,z1, x2,y2,z2;
	float A,B,C;
};

void printVertex(Vertex* v);
void printEdge(Edge* edge);
void sortThreeVerticesInPlace(Vertex *vertices);
void createEdgesInCCWdirection(Edge* edges, Vertex* verices);
void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* verices);
void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD);
short ctoi(float color);

int GzNewRender(GzRender **render, GzRenderClass renderClass, GzDisplay *display)
{
	/*
	- malloc a renderer struct
	- keep closed until BeginRender inits are done
	- span interpolator needs pointer to display for pixel writes
	- check for legal class GZ_Z_BUFFER_RENDER
	*/
	*render = (GzRender *) malloc(sizeof(GzRender));
	if (*render == NULL) return GZ_FAILURE;
	if (renderClass != GZ_Z_BUFFER_RENDER) return GZ_FAILURE;

	(*render)->display = display;
	(*render)->renderClass = renderClass;
	(*render)->open = 0;
	return GZ_SUCCESS;
}


int GzFreeRender(GzRender *render)
{
/* 
-free all renderer resources
*/
	if (render == NULL) return GZ_FAILURE;
	render->display = NULL;
	free(render);
	return GZ_SUCCESS;
}


int GzBeginRender(GzRender	*render)
{
/* 
- set up for start of each frame - init frame buffer
*/
	if (render == NULL) return GZ_FAILURE;
	// Setting open to true
	render->open = 1;
	// Setting the default animation color to be gray
	render->flatcolor[0] = 0.5f;
	render->flatcolor[1] = 0.5f;
	render->flatcolor[2] = 0.5f;
	return GZ_SUCCESS;
}


int GzPutAttribute(GzRender	*render, int numAttributes, GzToken	*nameList, 
	GzPointer *valueList) /* void** valuelist */
{
	/*
		- set renderer attribute states (e.g.: GZ_RGB_COLOR default color)
		- later set shaders, interpolaters, texture maps, and lights
	*/
	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;

    for (int i = 0; i < numAttributes; i++) {
    	if (nameList[i] == GZ_RGB_COLOR) {
    		GzColor* color = (GzColor*) valueList[i];
    		render->flatcolor[0] = (*color)[0];
    		render->flatcolor[1] = (*color)[1];
    		render->flatcolor[2] = (*color)[2];
    	}
    }
	return GZ_SUCCESS;
}


int GzPutTriangle(GzRender *render, int	numParts, GzToken *nameList,
	GzPointer *valueList) /* numParts - how many names and values */
{
	/*
	- pass in a triangle description with tokens and values corresponding to
		  GZ_NULL_TOKEN:		do nothing - no values
		  GZ_POSITION:		3 vert positions in model space
	- Invoke the scan converter and return an error code
	*/
	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;
	for (int j = 0; j < numParts; j++) {
		if (nameList[j] == GZ_POSITION) {
			Vertex vertices[3];
			Edge edges[3];
			GzCoord* coord = (GzCoord*) valueList[j];
			// Parse the vertices
			for (int i = 0; i < 3; i++) {
				vertices[i].x = (*coord)[3*i+0];
				vertices[i].y = (*coord)[3*i+1];
				vertices[i].z = (*coord)[3*i+2];
			}

			// Find equation of the plane in which triangle is present.
			// Ax + By + Cz + D = 0;
			float zA, zB, zC, zD;
			findPlaneEquation(vertices, &zA, &zB, &zC, &zD);

			// Sort the vertices in the ascending order of Y coordinate
			sortThreeVerticesInPlace(vertices);

			// Create edges such that they are in Clockwise/CounterClockwise direction
			createEdgesInCCWdirection(edges, vertices);

			// Find the bounding box for the triangle
			float minX, minY, maxX, maxY;
			findBoundingBox(&minX, &minY, &maxX, &maxY, vertices);

			// Iterate over all the pixels in the bounding box
			// On every pixel evaluate edge function for all three edges
			// If signs of all the three edge functions are same for a given pixel
			// that pixel is inside the triangle. We should render that pixel
			for (int x = floor(minX); x < ceil(maxX); x++) {
				for (int y = floor(minY); y < ceil(maxY); y++) {
					if ((
						  (edges[0].A * x + edges[0].B * y + edges[0].C) >= 0  &&
					      (edges[1].A * x + edges[1].B * y + edges[1].C) >= 0  &&
						  (edges[2].A * x + edges[2].B * y + edges[2].C) >= 0
						) ||
						(
						  (edges[0].A * x + edges[0].B * y + edges[0].C) <= 0  &&
						  (edges[1].A * x + edges[1].B * y + edges[1].C) <= 0  &&
						  (edges[2].A * x + edges[2].B * y + edges[2].C) <= 0
						)
					)
					{
						GzIntensity r,g,b,a;
						int previousDepth;
						GzGetDisplay(render->display, x, y, &r, &g ,&b, &a, &previousDepth);
						int z = (-1*zD-1*zB*y-1*zA*x)/zC;
						if (previousDepth == 0 || previousDepth > z) {
							GzPutDisplay(render->display, x, y, (GzIntensity ) ctoi((float) render->flatcolor[0]), (GzIntensity ) ctoi((float) render->flatcolor[1]), (GzIntensity ) ctoi((float) render->flatcolor[2]), a, z);
						}
					}
				}
			}
		}
	}
	return GZ_SUCCESS;
}

/* NOT part of API - just for general assistance */
short ctoi(float color)		/* convert float color to GzIntensity short */
{
  return(short)((int)(color * ((1 << 12) - 1)));
}

void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD) {
	float x0,y0,z0,x1,y1,z1;
	x0 = vertices[1].x - vertices[0].x;
	x1 = vertices[2].x - vertices[0].x;
	y0 = vertices[1].y - vertices[0].y;
	y1 = vertices[2].y - vertices[0].y;
	z0 = vertices[1].z - vertices[0].z;
	z1 = vertices[2].z - vertices[0].z;

	*zA = y0*z1-z0*y1;
	*zB = x1*z0-x0*z1;
	*zC = x0*y1-y0*x1;
	*zD = -(*zA)*vertices[0].x - (*zB)*vertices[0].y - (*zC)*vertices[0].z;
}

void printVertex(Vertex* v) {
	printf("EDGE = (%.3f , %.3f, %.3f) \n", v->x, v->y, v->z);
}

void printEdge(Edge* edge) {
	printf("EDGE = (%.3f , %.3f, %.3f) TO (%.3f, %.3f, %.3f) \n", edge->x1, edge->y1, edge->z1, edge->x2, edge->y2, edge->z2);
}


void sortThreeVerticesInPlace(Vertex *vertices) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	float y1 = v1->y;
	float y2 = v2->y;
	float y3 = v3->y;
	if (y1 < y2) {
		if (y1 < y3) {
			if (y2 < y3) {
				// 1 < 2 < 3
				return;
			} else {
				// 1 < 3 < 2
				Vertex tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
			}
		} else {
			// 3 < 1 < 2
			Vertex tmp = *v3;
			*v3 = *v1;
			*v1 = tmp;
			tmp = *v3;
			*v3 = *v2;
			*v2 = tmp;
		}
	} else {
		if (y2 < y3) {
			if (y3 < y1) {
				// 2 < 3 < 1
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
				tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
			} else {
				// 2 < 1 < 3
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
			}
		} else {
			// 3 < 2 < 1
			Vertex tmp = *v1;
			*v1 = *v3;
			*v3 = tmp;
		}
	}
}

void createEdgesInCCWdirection(Edge* edges, Vertex* vertices) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	// Find equation Ax + By + C = 0 for edge 1 - 3
    float A = (v3->y - v1->y) / (v3->x - v1->x);
    float C = v1->y - A * v1->x;
    float B = -1;
    // Find the value of x for y=v2->y;
    float x = (-B*v2->y - C)/ A;
    if (x < v2->x) { // It means that v2 is present on the right side
    	// CW direction is (1-2) , (2-3), (3-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v2->x; edges[0].y2 = v2->y; edges[0].z2 = v2->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v2->x; edges[1].y1 = v2->y; edges[1].z1 = v2->z; edges[1].x2 = v3->x; edges[1].y2 = v3->y; edges[1].z2 = v3->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v2->y - (edges[1].y2 - edges[1].y1) * v2->x;
    	edges[2].x1 = v3->x; edges[2].y1 = v3->y; edges[2].z1 = v3->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v3->y - (edges[2].y2 - edges[2].y1) * v3->x;
    } else {
    	// CW direction is (1-3) , (3-2), (2-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v3->x; edges[0].y2 = v3->y; edges[0].z2 = v3->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v3->x; edges[1].y1 = v3->y; edges[1].z1 = v3->z; edges[1].x2 = v2->x; edges[1].y2 = v2->y; edges[1].z2 = v2->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v3->y - (edges[1].y2 - edges[1].y1) * v3->x;
    	edges[2].x1 = v2->x; edges[2].y1 = v2->y; edges[2].z1 = v2->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v2->y - (edges[2].y2 - edges[2].y1) * v2->x;
    }
}

void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* vertices) {
	*minX = vertices[0].x;
	*minY = vertices[0].y;
	*maxX = vertices[0].x;
	*maxY = vertices[0].y;
	for (int i = 0; i < 3; i++) {
		if (vertices[i].x < *minX) *minX = vertices[i].x;
		if (vertices[i].y < *minY) *minY = vertices[i].y;
		if (vertices[i].x > *maxX) *maxX = vertices[i].x;
		if (vertices[i].y > *maxY) *maxY = vertices[i].y;
	}
}
