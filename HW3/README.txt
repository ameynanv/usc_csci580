CSCI 580 Homework 3
=====================
AMEY VAIDYA (USCID: 2836139499) Email: ameynanv@usc.edu
=====================
1) Steps to executing code without extra credit
	1) Locate Application3.cpp Line number 15-17.
	2) To render the output in default configuration set ANIMATE = 0, EXTRA_CREDIT_1_1 = 0 , EXTRA_CREDIT_1_1 = 0
	3) compile the code using make and run the program

2) To execute extra credit 1 output 1
	1) Locate Application3.cpp Line number 15-17.
	2) To render the extra credit 1 output 1 set ANIMATE = 0, EXTRA_CREDIT_1_1 = 1 , EXTRA_CREDIT_1_1 = 0
	3) compile the code using make and run the program

3) To execute extra credit 1 output 2
	1) Locate Application3.cpp Line number 15-17.
	2) To render the extra credit 1 output 2 set ANIMATE = 0, EXTRA_CREDIT_1_1 = 0 , EXTRA_CREDIT_1_1 = 1
	3) compile the code using make and run the program

4) To execute animation
	1) Locate Application3.cpp Line number 15-17.
	2) To render the animation set ANIMATE = 1, EXTRA_CREDIT_1_1 = 0 , EXTRA_CREDIT_1_1 = 0
	3) compile the code using make and run the program
	4) This will generate output files from output0000.ppm till output0099.ppm 
	