/* CS580 Homework 3 */

#include	<stdio.h>
#include	<math.h>
#include	"gz.h"
#include	"rend.h"
#include    <limits.h>
#include 	<stdlib.h>
#include 	<iostream>

struct Vertex {
	float x,y,z;
};

struct Edge {
	float x1,y1,z1, x2,y2,z2;
	float A,B,C;
};

void printVertex(Vertex* v);
void printEdge(Edge* edge);
void sortThreeVerticesInPlace(Vertex *vertices);
void createEdgesInCCWdirection(Edge* edges, Vertex* verices);
void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* verices);
void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD);
short ctoi(float color);

int GzRotXMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along x axis
// Pass back the matrix using mat value
	mat[0][0] = 1.0;
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] =  cos(degree * M_PI / 180);
	mat[1][2] = -sin(degree * M_PI / 180);
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] =  sin(degree * M_PI / 180);
	mat[2][2] =  cos(degree * M_PI / 180);
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzRotYMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along y axis
// Pass back the matrix using mat value
	mat[0][0] = cos(degree * M_PI / 180);
	mat[0][1] = 0.0;
	mat[0][2] = sin(degree * M_PI / 180);
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] = 1.0;
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = -sin(degree * M_PI / 180);
	mat[2][1] = 0.0;
	mat[2][2] = cos(degree * M_PI / 180);
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzRotZMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along z axis
// Pass back the matrix using mat value
	mat[0][0] = cos(degree * M_PI / 180);
	mat[0][1] = -sin(degree * M_PI / 180);
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = sin(degree * M_PI / 180);
	mat[1][1] = cos(degree * M_PI / 180);
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = 1.0;
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzTrxMat(GzCoord translate, GzMatrix mat)
{
// Create translation matrix
// Pass back the matrix using mat value
	mat[0][0] = 1.0;
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = translate[X];
	mat[1][0] = 0.0;
	mat[1][1] = 1.0;
	mat[1][2] = 0.0;
	mat[1][3] = translate[Y];
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = 1.0;
	mat[2][3] = translate[Z];
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzScaleMat(GzCoord scale, GzMatrix mat)
{
// Create scaling matrix
// Pass back the matrix using mat value
	mat[0][0] = scale[X];
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] = scale[Y];
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = scale[Z];
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


//----------------------------------------------------------
// Begin main functions

int GzNewRender(GzRender **render, GzRenderClass renderClass, GzDisplay	*display)
{
/*  
- malloc a renderer struct 
- keep closed until all inits are done 
- setup Xsp and anything only done once 
- span interpolator needs pointer to display 
- check for legal class GZ_Z_BUFFER_RENDER 
- init default camera 
*/ 
	*render = (GzRender *) malloc(sizeof(GzRender));
	if (*render == NULL) return GZ_FAILURE;
	if (renderClass != GZ_Z_BUFFER_RENDER) return GZ_FAILURE;
	if (display == NULL) return GZ_FAILURE;

	(*render)->camera.position[X] = DEFAULT_IM_X;
	(*render)->camera.position[Y] = DEFAULT_IM_Y;
	(*render)->camera.position[Z] = DEFAULT_IM_Z;
	(*render)->camera.lookat[X] = 0.0;
	(*render)->camera.lookat[Y] = 0.0;
	(*render)->camera.lookat[Z] = 0.0;
	(*render)->camera.worldup[X] = 0.0;
	(*render)->camera.worldup[Y] = 1.0;
	(*render)->camera.worldup[Z] = 0.0;
	(*render)->camera.FOV = DEFAULT_FOV;
	(*render)->matlevel = 0;

	double d = 1.00 / tan(0.5 * (*render)->camera.FOV*M_PI/180.00);

	(*render)->display = display;
	(*render)->renderClass = renderClass;
	(*render)->open = 0;

	(*render)->Xsp[0][0] =  (*render)->display->xres / 2;
	(*render)->Xsp[0][1] =  0.0;
	(*render)->Xsp[0][2] =  0.0;
	(*render)->Xsp[0][3] =  (*render)->display->xres / 2;

	(*render)->Xsp[1][0] =  0.0;
	(*render)->Xsp[1][1] =  -1 * (*render)->display->yres / 2;
	(*render)->Xsp[1][2] =  0.0;
	(*render)->Xsp[1][3] =  (*render)->display->yres / 2;

	(*render)->Xsp[2][0] =  0.0;
	(*render)->Xsp[2][1] =  0.0;
	(*render)->Xsp[2][2] =  INT_MAX/d;
	(*render)->Xsp[2][3] =  0.0;

	(*render)->Xsp[3][0] =  0.0;
	(*render)->Xsp[3][1] =  0.0;
	(*render)->Xsp[3][2] =  0.0;
	(*render)->Xsp[3][3] =  1.0;
	return GZ_SUCCESS;
}


int GzFreeRender(GzRender *render)
{
/* 
-free all renderer resources
*/
	if (render == NULL) return GZ_FAILURE;
	render->display = NULL;
	free(render);
	return GZ_SUCCESS;
	return GZ_SUCCESS;
}


int GzBeginRender(GzRender *render)
{
/*  
- set up for start of each frame - clear frame buffer 
- compute Xiw and projection xform Xpi from camera definition 
- init Ximage - put Xsp at base of stack, push on Xpi and Xiw 
- now stack contains Xsw and app can push model Xforms if it want to. 
*/ 
	if (render == NULL) return GZ_FAILURE;
	// Setting open to true
	render->open = 1;
	// Setting the default animation color to be gray
	render->flatcolor[0] = 0.5f;
	render->flatcolor[1] = 0.5f;
	render->flatcolor[2] = 0.5f;

	double d = 1.00 / tan(0.5 * render->camera.FOV*M_PI/180.00);
	render->camera.Xpi[0][0] = 1.0; render->camera.Xpi[0][1] = 0.0; render->camera.Xpi[0][2] = 0.0; render->camera.Xpi[0][3] = 0.0;
	render->camera.Xpi[1][0] = 0.0; render->camera.Xpi[1][1] = 1.0; render->camera.Xpi[1][2] = 0.0; render->camera.Xpi[1][3] = 0.0;
	render->camera.Xpi[2][0] = 0.0; render->camera.Xpi[2][1] = 0.0; render->camera.Xpi[2][2] = 1.0; render->camera.Xpi[2][3] = 0.0;
	render->camera.Xpi[3][0] = 0.0; render->camera.Xpi[3][1] = 0.0; render->camera.Xpi[3][2] = 1.0/d; render->camera.Xpi[3][3] = 1.0;

	float mag;
	GzCoord _X,_Y,_Z;
	GzCoord _CL;
	_CL[X] = render->camera.lookat[X] - render->camera.position[X];
	_CL[Y] = render->camera.lookat[Y] - render->camera.position[Y];
	_CL[Z] = render->camera.lookat[Z] - render->camera.position[Z];
	mag = sqrt(_CL[X]*_CL[X] + _CL[Y]*_CL[Y] + _CL[Z]*_CL[Z]);
	_Z[X] = _CL[X]/mag; _Z[Y] = _CL[Y]/mag; _Z[Z] = _CL[Z]/mag;

	GzCoord _U;
	float up_dot_Z = render->camera.worldup[X] *_Z[X] + render->camera.worldup[Y] *_Z[Y] + render->camera.worldup[Z] * _Z[Z];
	_U[X] = render->camera.worldup[X] - up_dot_Z * _Z[X];
	_U[Y] = render->camera.worldup[Y] - up_dot_Z * _Z[Y];
	_U[Z] = render->camera.worldup[Z] - up_dot_Z * _Z[Z];
	mag = sqrt(_U[X]*_U[X] + _U[Y]*_U[Y] + _U[Z]*_U[Z]);

	_Y[X] = _U[X]/mag; _Y[Y] = _U[Y]/mag; _Y[Z] = _U[Z]/mag;

	_X[X] = _Y[Y]*_Z[Z] - _Y[Z]*_Z[Y];
	_X[Y] = _Y[Z]*_Z[X] - _Y[X]*_Z[Z];
	_X[Z] = _Y[X]*_Z[Y] - _Y[Y]*_Z[X];

	float X_dot_C = render->camera.position[X] * _X[X] + render->camera.position[Y] *_X[Y] + render->camera.position[Z] * _X[Z];
	float Y_dot_C = render->camera.position[X] * _Y[X] + render->camera.position[Y] *_Y[Y] + render->camera.position[Z] * _Y[Z];
	float Z_dot_C = render->camera.position[X] * _Z[X] + render->camera.position[Y] *_Z[Y] + render->camera.position[Z] * _Z[Z];

	render->camera.Xiw[0][0] = _X[X]; render->camera.Xiw[0][1] = _X[Y]; render->camera.Xiw[0][2] = _X[Z]; render->camera.Xiw[0][3] = -1*X_dot_C;
	render->camera.Xiw[1][0] = _Y[X]; render->camera.Xiw[1][1] = _Y[Y]; render->camera.Xiw[1][2] = _Y[Z]; render->camera.Xiw[1][3] = -1*Y_dot_C;
	render->camera.Xiw[2][0] = _Z[X]; render->camera.Xiw[2][1] = _Z[Y]; render->camera.Xiw[2][2] = _Z[Z]; render->camera.Xiw[2][3] = -1*Z_dot_C;
	render->camera.Xiw[3][0] = 0.0; render->camera.Xiw[3][1] = 0.0; render->camera.Xiw[3][2] = 0.0; render->camera.Xiw[3][3] = 1.0;

	GzPushMatrix(render,render->Xsp);
	GzPushMatrix(render, render->camera.Xpi);
	GzPushMatrix(render, render->camera.Xiw);
	return GZ_SUCCESS;
}

int GzPutCamera(GzRender *render, GzCamera *camera)
{
/*
- overwrite renderer camera structure with new camera definition
*/
	if (render == NULL || camera == NULL) return GZ_FAILURE;
	render->camera.FOV = camera->FOV;
	render->camera.position[X] = camera->position[X];
	render->camera.position[Y] = camera->position[Y];
	render->camera.position[Z] = camera->position[Z];
	render->camera.lookat[X] = camera->lookat[X];
	render->camera.lookat[Y] = camera->lookat[Y];
	render->camera.lookat[Z] = camera->lookat[Z];
	render->camera.worldup[X] = camera->worldup[X];
	render->camera.worldup[Y] = camera->worldup[Y];
	render->camera.worldup[Z] = camera->worldup[Z];

	double d = 1.00 / tan(0.5 * render->camera.FOV*M_PI/180.00);
	render->Xsp[2][2] = INT_MAX/d;
	return GZ_SUCCESS;	
}

int GzPushMatrix(GzRender *render, GzMatrix matrix)
{
/*
- push a matrix onto the Ximage stack
- check for stack overflow
*/
	if (render == NULL || matrix == NULL) return GZ_FAILURE;
	if (render->matlevel >= MATLEVELS) return GZ_FAILURE;

	if (render->matlevel == 0) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				render->Ximage[render->matlevel][i][j] = matrix[i][j];
			}
		}
	} else {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				render->Ximage[render->matlevel][i][j] = render->Ximage[render->matlevel - 1][i][0] * matrix[0][j] +
						                                 render->Ximage[render->matlevel - 1][i][1] * matrix[1][j] +
						                                 render->Ximage[render->matlevel - 1][i][2] * matrix[2][j] +
														 render->Ximage[render->matlevel - 1][i][3] * matrix[3][j];
			}
		}
	}
	render->matlevel = render->matlevel + 1;
	return GZ_SUCCESS;
}

int GzPopMatrix(GzRender *render)
{
/*
- pop a matrix off the Ximage stack
- check for stack underflow
*/
	if (render == NULL) return GZ_FAILURE;
	if (render->matlevel > 0) render->matlevel = render->matlevel - 1;
	return GZ_SUCCESS;
}


int GzPutAttribute(GzRender *render, int numAttributes, GzToken *nameList, 
	GzPointer *valueList) /* void** valuelist */
{
/*
- set renderer attribute states (e.g.: GZ_RGB_COLOR default color)
- later set shaders, interpolaters, texture maps, and lights
*/
	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;

    for (int i = 0; i < numAttributes; i++) {
    	if (nameList[i] == GZ_RGB_COLOR) {
    		GzColor* color = (GzColor*) valueList[i];
    		render->flatcolor[0] = (*color)[0];
    		render->flatcolor[1] = (*color)[1];
    		render->flatcolor[2] = (*color)[2];
    	}
    }
	return GZ_SUCCESS;
}

int GzPutTriangle(GzRender *render, int numParts, GzToken *nameList, 
				  GzPointer *valueList)
/* numParts : how many names and values */
{
/*  
- pass in a triangle description with tokens and values corresponding to 
      GZ_POSITION:3 vert positions in model space 
- Xform positions of verts  
- Clip - just discard any triangle with verts behind view plane 
       - test for triangles with all three verts off-screen 
- invoke triangle rasterizer  
*/
	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;
	for (int j = 0; j < numParts; j++) {
		if (nameList[j] == GZ_POSITION) {
			Vertex vertices[3];
			Edge edges[3];
			GzCoord* coord = (GzCoord*) valueList[j];
			// Parse the vertices
			for (int i = 0; i < 3; i++) {
				vertices[i].x = (*coord)[3*i+0];
				vertices[i].y = (*coord)[3*i+1];
				vertices[i].z = (*coord)[3*i+2];
			}

			for (int i = 0; i < 3; i++) {
				float _x, _y, _z, _w;
				_x = render->Ximage[render->matlevel - 1][0][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][0][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][0][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][0][3];
				_y = render->Ximage[render->matlevel - 1][1][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][1][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][1][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][1][3];
				_z = render->Ximage[render->matlevel - 1][2][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][2][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][2][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][2][3];
				_w = render->Ximage[render->matlevel - 1][3][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][3][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][3][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][3][3];
				vertices[i].x = _x / _w;
				vertices[i].y = _y / _w;
				vertices[i].z = _z / _w;

				if ((vertices[i].x < 0 || vertices[i].x > render->display->xres) &&
					(vertices[i].y < 0 || vertices[i].y > render->display->yres) &&
					(vertices[i].z < 0))
				{
					return GZ_FAILURE;
				}
			}


			// Find equation of the plane in which triangle is present.
			// Ax + By + Cz + D = 0;
			float zA, zB, zC, zD;
			findPlaneEquation(vertices, &zA, &zB, &zC, &zD);

			// Sort the vertices in the ascending order of Y coordinate
			sortThreeVerticesInPlace(vertices);

			// Create edges such that they are in Clockwise/CounterClockwise direction
			createEdgesInCCWdirection(edges, vertices);

			// Find the bounding box for the triangle
			float minX, minY, maxX, maxY;
			findBoundingBox(&minX, &minY, &maxX, &maxY, vertices);

			// Iterate over all the pixels in the bounding box
			// On every pixel evaluate edge function for all three edges
			// If signs of all the three edge functions are same for a given pixel
			// that pixel is inside the triangle. We should render that pixel
			for (int x = floor(minX); x < ceil(maxX); x++) {
				for (int y = floor(minY); y < ceil(maxY); y++) {
					if ((
						  (edges[0].A * x + edges[0].B * y + edges[0].C) >= 0  &&
						  (edges[1].A * x + edges[1].B * y + edges[1].C) >= 0  &&
						  (edges[2].A * x + edges[2].B * y + edges[2].C) >= 0
						) ||
						(
						  (edges[0].A * x + edges[0].B * y + edges[0].C) <= 0  &&
						  (edges[1].A * x + edges[1].B * y + edges[1].C) <= 0  &&
						  (edges[2].A * x + edges[2].B * y + edges[2].C) <= 0
						)
					)
					{
						GzIntensity r,g,b,a;
						int previousDepth;
						GzGetDisplay(render->display, x, y, &r, &g ,&b, &a, &previousDepth);
						int z = (-1*zD-1*zB*y-1*zA*x)/zC;
						if (previousDepth == 0 || previousDepth > z) {
							GzPutDisplay(render->display, x, y, (GzIntensity ) ctoi((float) render->flatcolor[0]), (GzIntensity ) ctoi((float) render->flatcolor[1]), (GzIntensity ) ctoi((float) render->flatcolor[2]), a, z);
						}
					}
				}
			}
		}
	}
	return GZ_SUCCESS;
}

/* NOT part of API - just for general assistance */

short	ctoi(float color)		/* convert float color to GzIntensity short */
{
  return(short)((int)(color * ((1 << 12) - 1)));
}


void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD) {
	float x0,y0,z0,x1,y1,z1;
	x0 = vertices[1].x - vertices[0].x;
	x1 = vertices[2].x - vertices[0].x;
	y0 = vertices[1].y - vertices[0].y;
	y1 = vertices[2].y - vertices[0].y;
	z0 = vertices[1].z - vertices[0].z;
	z1 = vertices[2].z - vertices[0].z;

	*zA = y0*z1-z0*y1;
	*zB = x1*z0-x0*z1;
	*zC = x0*y1-y0*x1;
	*zD = -(*zA)*vertices[0].x - (*zB)*vertices[0].y - (*zC)*vertices[0].z;
}

void printVertex(Vertex* v) {
	printf("VERTEX = (%.3f , %.3f, %.3f) \n", v->x, v->y, v->z);
}

void printEdge(Edge* edge) {
	printf("EDGE = (%.3f , %.3f, %.3f) TO (%.3f, %.3f, %.3f) \n", edge->x1, edge->y1, edge->z1, edge->x2, edge->y2, edge->z2);
}


void sortThreeVerticesInPlace(Vertex *vertices) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	float y1 = v1->y;
	float y2 = v2->y;
	float y3 = v3->y;
	if (y1 < y2) {
		if (y1 < y3) {
			if (y2 < y3) {
				// 1 < 2 < 3
				return;
			} else {
				// 1 < 3 < 2
				Vertex tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
			}
		} else {
			// 3 < 1 < 2
			Vertex tmp = *v3;
			*v3 = *v1;
			*v1 = tmp;
			tmp = *v3;
			*v3 = *v2;
			*v2 = tmp;
		}
	} else {
		if (y2 < y3) {
			if (y3 < y1) {
				// 2 < 3 < 1
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
				tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
			} else {
				// 2 < 1 < 3
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
			}
		} else {
			// 3 < 2 < 1
			Vertex tmp = *v1;
			*v1 = *v3;
			*v3 = tmp;
		}
	}
}

void createEdgesInCCWdirection(Edge* edges, Vertex* vertices) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	// Find equation Ax + By + C = 0 for edge 1 - 3
    float A = (v3->y - v1->y) / (v3->x - v1->x);
    float C = v1->y - A * v1->x;
    float B = -1;
    // Find the value of x for y=v2->y;
    float x = (-B*v2->y - C)/ A;
    if (x < v2->x) { // It means that v2 is present on the right side
    	// CW direction is (1-2) , (2-3), (3-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v2->x; edges[0].y2 = v2->y; edges[0].z2 = v2->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v2->x; edges[1].y1 = v2->y; edges[1].z1 = v2->z; edges[1].x2 = v3->x; edges[1].y2 = v3->y; edges[1].z2 = v3->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v2->y - (edges[1].y2 - edges[1].y1) * v2->x;
    	edges[2].x1 = v3->x; edges[2].y1 = v3->y; edges[2].z1 = v3->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v3->y - (edges[2].y2 - edges[2].y1) * v3->x;
    } else {
    	// CW direction is (1-3) , (3-2), (2-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v3->x; edges[0].y2 = v3->y; edges[0].z2 = v3->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v3->x; edges[1].y1 = v3->y; edges[1].z1 = v3->z; edges[1].x2 = v2->x; edges[1].y2 = v2->y; edges[1].z2 = v2->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v3->y - (edges[1].y2 - edges[1].y1) * v3->x;
    	edges[2].x1 = v2->x; edges[2].y1 = v2->y; edges[2].z1 = v2->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v2->y - (edges[2].y2 - edges[2].y1) * v2->x;
    }
}

void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* vertices) {
	*minX = vertices[0].x;
	*minY = vertices[0].y;
	*maxX = vertices[0].x;
	*maxY = vertices[0].y;
	for (int i = 0; i < 3; i++) {
		if (vertices[i].x < *minX) *minX = vertices[i].x;
		if (vertices[i].y < *minY) *minY = vertices[i].y;
		if (vertices[i].x > *maxX) *maxX = vertices[i].x;
		if (vertices[i].y > *maxY) *maxY = vertices[i].y;
	}
}

