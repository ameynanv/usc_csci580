/* CS580 Homework 4 */

#include	<stdio.h>
#include	<math.h>
#include	"gz.h"
#include	"rend.h"
#include    <limits.h>
#include 	<stdlib.h>
#include 	<iostream>

struct Vertex {
	float x,y,z;
};

struct Edge {
	float x1,y1,z1, x2,y2,z2;
	float A,B,C;
};

void printVertex(Vertex* v);
void printEdge(Edge* edge);
void sortThreeVerticesInPlace(Vertex *vertices, Vertex *colors);
void createEdgesInCCWdirection(Edge* edges, Vertex* verices);
void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* verices);
void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD);
short ctoi(float color);

int GzRotXMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along x axis
// Pass back the matrix using mat value
	mat[0][0] = 1.0;
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] =  cos(degree * M_PI / 180);
	mat[1][2] = -sin(degree * M_PI / 180);
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] =  sin(degree * M_PI / 180);
	mat[2][2] =  cos(degree * M_PI / 180);
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzRotYMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along y axis
// Pass back the matrix using mat value
	mat[0][0] = cos(degree * M_PI / 180);
	mat[0][1] = 0.0;
	mat[0][2] = sin(degree * M_PI / 180);
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] = 1.0;
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = -sin(degree * M_PI / 180);
	mat[2][1] = 0.0;
	mat[2][2] = cos(degree * M_PI / 180);
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzRotZMat(float degree, GzMatrix mat)
{
// Create rotate matrix : rotate along z axis
// Pass back the matrix using mat value
	mat[0][0] = cos(degree * M_PI / 180);
	mat[0][1] = -sin(degree * M_PI / 180);
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = sin(degree * M_PI / 180);
	mat[1][1] = cos(degree * M_PI / 180);
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = 1.0;
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzTrxMat(GzCoord translate, GzMatrix mat)
{
// Create translation matrix
// Pass back the matrix using mat value
	mat[0][0] = 1.0;
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = translate[X];
	mat[1][0] = 0.0;
	mat[1][1] = 1.0;
	mat[1][2] = 0.0;
	mat[1][3] = translate[Y];
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = 1.0;
	mat[2][3] = translate[Z];
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


int GzScaleMat(GzCoord scale, GzMatrix mat)
{
// Create scaling matrix
// Pass back the matrix using mat value
	mat[0][0] = scale[X];
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
	mat[1][0] = 0.0;
	mat[1][1] = scale[Y];
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = scale[Z];
	mat[2][3] = 0.0;
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
	return GZ_SUCCESS;
}


//----------------------------------------------------------
// Begin main functions

int GzNewRender(GzRender **render, GzRenderClass renderClass, GzDisplay	*display)
{
/*  
- malloc a renderer struct 
- keep closed until all inits are done 
- setup Xsp and anything only done once 
- span interpolator needs pointer to display 
- check for legal class GZ_Z_BUFFER_RENDER 
- init default camera 
*/ 
	*render = (GzRender *) malloc(sizeof(GzRender));
	if (*render == NULL) return GZ_FAILURE;
	if (renderClass != GZ_Z_BUFFER_RENDER) return GZ_FAILURE;
	if (display == NULL) return GZ_FAILURE;

	(*render)->camera.position[X] = DEFAULT_IM_X;
	(*render)->camera.position[Y] = DEFAULT_IM_Y;
	(*render)->camera.position[Z] = DEFAULT_IM_Z;
	(*render)->camera.lookat[X] = 0.0;
	(*render)->camera.lookat[Y] = 0.0;
	(*render)->camera.lookat[Z] = 0.0;
	(*render)->camera.worldup[X] = 0.0;
	(*render)->camera.worldup[Y] = 1.0;
	(*render)->camera.worldup[Z] = 0.0;
	(*render)->camera.FOV = DEFAULT_FOV;

	(*render)->matlevel = 0;

	double d = 1.00 / tan(0.5 * (*render)->camera.FOV*M_PI/180.00);

	(*render)->display = display;
	(*render)->renderClass = renderClass;
	(*render)->open = 0;

	(*render)->Xsp[0][0] =  (*render)->display->xres / 2;
	(*render)->Xsp[0][1] =  0.0;
	(*render)->Xsp[0][2] =  0.0;
	(*render)->Xsp[0][3] =  (*render)->display->xres / 2;

	(*render)->Xsp[1][0] =  0.0;
	(*render)->Xsp[1][1] =  -1 * (*render)->display->yres / 2;
	(*render)->Xsp[1][2] =  0.0;
	(*render)->Xsp[1][3] =  (*render)->display->yres / 2;

	(*render)->Xsp[2][0] =  0.0;
	(*render)->Xsp[2][1] =  0.0;
	(*render)->Xsp[2][2] =  INT_MAX/d;
	(*render)->Xsp[2][3] =  0.0;

	(*render)->Xsp[3][0] =  0.0;
	(*render)->Xsp[3][1] =  0.0;
	(*render)->Xsp[3][2] =  0.0;
	(*render)->Xsp[3][3] =  1.0;

	// HW4
	(*render)->numlights = 0;
	(*render)->interp_mode = GZ_COLOR;
	//TODO Set default lights and coefficients

	return GZ_SUCCESS;
}


int GzFreeRender(GzRender *render)
{
/* 
-free all renderer resources
*/
	if (render == NULL) return GZ_FAILURE;
	render->display = NULL;
	free(render);
	return GZ_SUCCESS;
	return GZ_SUCCESS;
}


int GzBeginRender(GzRender *render)
{
/*  
- set up for start of each frame - clear frame buffer 
- compute Xiw and projection xform Xpi from camera definition 
- init Ximage - put Xsp at base of stack, push on Xpi and Xiw 
- now stack contains Xsw and app can push model Xforms if it want to. 
*/ 
	if (render == NULL) return GZ_FAILURE;
	// Setting open to true
	render->open = 1;
	// Setting the default animation color to be gray
	render->flatcolor[0] = 0.5f;
	render->flatcolor[1] = 0.5f;
	render->flatcolor[2] = 0.5f;

	double d = 1.00 / tan(0.5 * render->camera.FOV*M_PI/180.00);
	render->camera.Xpi[0][0] = 1.0; render->camera.Xpi[0][1] = 0.0; render->camera.Xpi[0][2] = 0.0; render->camera.Xpi[0][3] = 0.0;
	render->camera.Xpi[1][0] = 0.0; render->camera.Xpi[1][1] = 1.0; render->camera.Xpi[1][2] = 0.0; render->camera.Xpi[1][3] = 0.0;
	render->camera.Xpi[2][0] = 0.0; render->camera.Xpi[2][1] = 0.0; render->camera.Xpi[2][2] = 1.0; render->camera.Xpi[2][3] = 0.0;
	render->camera.Xpi[3][0] = 0.0; render->camera.Xpi[3][1] = 0.0; render->camera.Xpi[3][2] = 1.0/d; render->camera.Xpi[3][3] = 1.0;

	float mag;
	GzCoord _X,_Y,_Z;
	GzCoord _CL;
	_CL[X] = render->camera.lookat[X] - render->camera.position[X];
	_CL[Y] = render->camera.lookat[Y] - render->camera.position[Y];
	_CL[Z] = render->camera.lookat[Z] - render->camera.position[Z];
	mag = sqrt(_CL[X]*_CL[X] + _CL[Y]*_CL[Y] + _CL[Z]*_CL[Z]);
	_Z[X] = _CL[X]/mag; _Z[Y] = _CL[Y]/mag; _Z[Z] = _CL[Z]/mag;

	GzCoord _U;
	float up_dot_Z = render->camera.worldup[X] *_Z[X] + render->camera.worldup[Y] *_Z[Y] + render->camera.worldup[Z] * _Z[Z];
	_U[X] = render->camera.worldup[X] - up_dot_Z * _Z[X];
	_U[Y] = render->camera.worldup[Y] - up_dot_Z * _Z[Y];
	_U[Z] = render->camera.worldup[Z] - up_dot_Z * _Z[Z];
	mag = sqrt(_U[X]*_U[X] + _U[Y]*_U[Y] + _U[Z]*_U[Z]);

	_Y[X] = _U[X]/mag; _Y[Y] = _U[Y]/mag; _Y[Z] = _U[Z]/mag;

	_X[X] = _Y[Y]*_Z[Z] - _Y[Z]*_Z[Y];
	_X[Y] = _Y[Z]*_Z[X] - _Y[X]*_Z[Z];
	_X[Z] = _Y[X]*_Z[Y] - _Y[Y]*_Z[X];

	float X_dot_C = render->camera.position[X] * _X[X] + render->camera.position[Y] *_X[Y] + render->camera.position[Z] * _X[Z];
	float Y_dot_C = render->camera.position[X] * _Y[X] + render->camera.position[Y] *_Y[Y] + render->camera.position[Z] * _Y[Z];
	float Z_dot_C = render->camera.position[X] * _Z[X] + render->camera.position[Y] *_Z[Y] + render->camera.position[Z] * _Z[Z];

	render->camera.Xiw[0][0] = _X[X]; render->camera.Xiw[0][1] = _X[Y]; render->camera.Xiw[0][2] = _X[Z]; render->camera.Xiw[0][3] = -1*X_dot_C;
	render->camera.Xiw[1][0] = _Y[X]; render->camera.Xiw[1][1] = _Y[Y]; render->camera.Xiw[1][2] = _Y[Z]; render->camera.Xiw[1][3] = -1*Y_dot_C;
	render->camera.Xiw[2][0] = _Z[X]; render->camera.Xiw[2][1] = _Z[Y]; render->camera.Xiw[2][2] = _Z[Z]; render->camera.Xiw[2][3] = -1*Z_dot_C;
	render->camera.Xiw[3][0] = 0.0; render->camera.Xiw[3][1] = 0.0; render->camera.Xiw[3][2] = 0.0; render->camera.Xiw[3][3] = 1.0;

	GzPushMatrix(render,render->Xsp);
	GzPushMatrix(render, render->camera.Xpi);
	GzPushMatrix(render, render->camera.Xiw);
	return GZ_SUCCESS;
}

int GzPutCamera(GzRender *render, GzCamera *camera)
{
/*
- overwrite renderer camera structure with new camera definition
*/
	if (render == NULL || camera == NULL) return GZ_FAILURE;
	render->camera.FOV = camera->FOV;
	render->camera.position[X] = camera->position[X];
	render->camera.position[Y] = camera->position[Y];
	render->camera.position[Z] = camera->position[Z];
	render->camera.lookat[X] = camera->lookat[X];
	render->camera.lookat[Y] = camera->lookat[Y];
	render->camera.lookat[Z] = camera->lookat[Z];
	render->camera.worldup[X] = camera->worldup[X];
	render->camera.worldup[Y] = camera->worldup[Y];
	render->camera.worldup[Z] = camera->worldup[Z];

	double d = 1.00 / tan(0.5 * render->camera.FOV*M_PI/180.00);
	render->Xsp[2][2] = INT_MAX/d;
	return GZ_SUCCESS;	
}

int GzPushMatrix(GzRender *render, GzMatrix matrix)
{
/*
- push a matrix onto the Ximage stack
- check for stack overflow
*/
	if (render == NULL || matrix == NULL) return GZ_FAILURE;
	if (render->matlevel >= MATLEVELS) return GZ_FAILURE;

	if (render->matlevel == 0) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				render->Ximage[render->matlevel][i][j] = matrix[i][j];
			}
		}
	} else {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				render->Ximage[render->matlevel][i][j] = render->Ximage[render->matlevel - 1][i][0] * matrix[0][j] +
						                                 render->Ximage[render->matlevel - 1][i][1] * matrix[1][j] +
						                                 render->Ximage[render->matlevel - 1][i][2] * matrix[2][j] +
														 render->Ximage[render->matlevel - 1][i][3] * matrix[3][j];
			}
		}
	}

	// Special case for handling normals
	// matlevel == 0 , it means that it is Xsp transform
	// matlevel == 1 , it means that it is Xpi transform
	// We should consider only matlevel == 2 and onwards for normals
	if (render->matlevel == 0 || render->matlevel == 1) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == j) {
					render->Xnorm[render->matlevel][i][j] = 1;
				} else {
					render->Xnorm[render->matlevel][i][j] = 0;
				}
			}
		}
	} else {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (j == 3) {
					render->Xnorm[render->matlevel][i][j] = render->Xnorm[render->matlevel - 1][i][3];
				} else {
					render->Xnorm[render->matlevel][i][j] = render->Xnorm[render->matlevel - 1][i][0] * matrix[0][j] +
							                                 render->Xnorm[render->matlevel - 1][i][1] * matrix[1][j] +
							                                 render->Xnorm[render->matlevel - 1][i][2] * matrix[2][j] +
															 render->Xnorm[render->matlevel - 1][i][3] * matrix[3][j];
				}
			}
		}
		float rowMag = sqrt(render->Xnorm[render->matlevel][0][0] * render->Xnorm[render->matlevel][0][0] +
							render->Xnorm[render->matlevel][0][1] * render->Xnorm[render->matlevel][0][1] +
							render->Xnorm[render->matlevel][0][2] * render->Xnorm[render->matlevel][0][2]);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				render->Xnorm[render->matlevel][i][j] = render->Xnorm[render->matlevel][i][j] / rowMag;
			}
		}
	}

	render->matlevel = render->matlevel + 1;
	return GZ_SUCCESS;
}

int GzPopMatrix(GzRender *render)
{
/*
- pop a matrix off the Ximage stack
- check for stack underflow
*/
	if (render == NULL) return GZ_FAILURE;
	if (render->matlevel > 0) render->matlevel = render->matlevel - 1;
	return GZ_SUCCESS;
}


int GzPutAttribute(GzRender *render, int numAttributes, GzToken *nameList, 
	GzPointer *valueList) /* void** valuelist */
{
/*
- set renderer attribute states (e.g.: GZ_RGB_COLOR default color)
- later set shaders, interpolaters, texture maps, and lights
*/
	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;

    for (int i = 0; i < numAttributes; i++) {
    	if (nameList[i] == GZ_RGB_COLOR) {
    		GzColor* color = (GzColor*) valueList[i];
    		render->flatcolor[X] = (*color)[X];
    		render->flatcolor[Y] = (*color)[Y];
    		render->flatcolor[Z] = (*color)[Z];
    	} 

		else if (nameList[i] == GZ_INTERPOLATE) {
    		int *intPointer =  (int*) valueList[i];
    		render->interp_mode = *(intPointer);
#if DEBUG
    		printf("GZ_INTERPOLATE = %d\n", render->interp_mode);
#endif
    	} else if (nameList[i] == GZ_DIRECTIONAL_LIGHT) {
    		if( render->numlights < MAX_LIGHTS ) {
				GzLight *light = (GzLight *)valueList[i];
				render->lights[render->numlights].color[X] = light->color[X];
				render->lights[render->numlights].color[Y] = light->color[Y];
				render->lights[render->numlights].color[Z] = light->color[Z];
				render->lights[render->numlights].direction[X] = light->direction[X];
				render->lights[render->numlights].direction[Y] = light->direction[Y];
				render->lights[render->numlights].direction[Z] = light->direction[Z];
#if DEBUG
				printf("GZ_DIRECTIONAL_LIGHT = %.2f %.2f %.2f ; %.2f %.2f %.2f\n", render->lights[render->numlights].direction[X], render->lights[render->numlights].direction[Y], render->lights[render->numlights].direction[Z], render->lights[render->numlights].color[X], render->lights[render->numlights].color[Y], render->lights[render->numlights].color[Z]);
#endif
				render->numlights++;
			}
    	} else if (nameList[i] == GZ_AMBIENT_LIGHT) {
            GzLight * ambientLight = (GzLight *)valueList[i];
			render->ambientlight.color[X] = ambientLight->color[X];
			render->ambientlight.color[Y] = ambientLight->color[Y];
			render->ambientlight.color[Z] = ambientLight->color[Z];
#if DEBUG
			printf("GZ_AMBIENT_LIGHT = %.2f %.2f %.2f\n", render->ambientlight.color[X], render->ambientlight.color[Y] , render->ambientlight.color[Z]);
#endif
    	} else if (nameList[i] == GZ_DIFFUSE_COEFFICIENT) {
    		GzColor* kd = (GzColor*) valueList[i];
    		render->Kd[X] = (*kd)[X];
    		render->Kd[Y] = (*kd)[Y];
    		render->Kd[Z] = (*kd)[Z];
#if DEBUG
    		printf("GZ_DIFFUSE_COEFFICIENT = %.2f %.2f %.2f\n", render->Kd[X], render->Kd[Y] , render->Kd[Z]);
#endif
    	} else if (nameList[i] == GZ_AMBIENT_COEFFICIENT) {
    		GzColor* ka = (GzColor*) valueList[i];
    		render->Ka[X] = (*ka)[X];
    		render->Ka[Y] = (*ka)[Y];
    		render->Ka[Z] = (*ka)[Z];
#if DEBUG
    		printf("GZ_AMBIENT_COEFFICIENT = %.2f %.2f %.2f\n", render->Ka[X], render->Ka[Y] , render->Ka[Z]);
#endif
    	} else if (nameList[i] == GZ_SPECULAR_COEFFICIENT) {
    		GzColor* ks = (GzColor*) valueList[i];
    		render->Ks[X] = (*ks)[X];
    		render->Ks[Y] = (*ks)[Y];
    		render->Ks[Z] = (*ks)[Z];
#if DEBUG
    		printf("GZ_SPECULAR_COEFFICIENT = %.2f %.2f %.2f\n", render->Ks[X], render->Ks[Y] , render->Ks[Z]);
#endif
    	} else if (nameList[i] == GZ_DISTRIBUTION_COEFFICIENT) {
    		float* s = (float*) valueList[i];
    		render->spec = *s;
#if DEBUG
    		printf("GZ_DISTRIBUTION_COEFFICIENT = %.2f\n", render->spec);
#endif
    	}
    }
	return GZ_SUCCESS;
}

void getColorFromNormal(GzRender *render, Vertex *normal, Vertex *vertexColor) {

	GzCoord L, E, N, R;
	E[X] = 0; E[Y] = 0; E[Z] = -1;
	N[X] = normal->x; N[Y] = normal->y; N[Z] = normal->z;

	GzCoord specular, diffuse;
	specular[X] = 0; specular[Y] = 0; specular[Z] = 0;
	diffuse[X] = 0; diffuse[Y] = 0; diffuse[Z] = 0;

	for (int i = 0; i < render->numlights; i++) {
		L[X] = render->lights[i].direction[X]; L[Y] = render->lights[i].direction[Y]; L[Z] = render->lights[i].direction[Z];
		float NdotL = N[X]*L[X]+N[Y]*L[Y]+N[Z]*L[Z];
		float NdotE = N[X]*E[X]+N[Y]*E[Y]+N[Z]*E[Z];
		float RdotE;
		if (NdotL > 0 && NdotE > 0) {
			// Everything is correct
		} else if (NdotL < 0 && NdotE < 0) {
			// Flip normal
			N[X] = -normal->x; N[Y] = -normal->y; N[Z] = -normal->z;
			NdotL = NdotL * -1;
			NdotE = NdotE * -1;
		} else {
			// Ignore this light calculation
			continue;
		}
		R[X] = 2 * NdotL * N[X] - render->lights[i].direction[X];
		R[Y] = 2 * NdotL * N[Y] - render->lights[i].direction[Y];
		R[Z] = 2 * NdotL * N[Z] - render->lights[i].direction[Z];
		float mag = sqrt(R[X]*R[X] + R[Y]*R[Y] + R[Z]*R[Z]);
		R[X] /= mag; R[Y] /= mag; R[Z] /= mag;

		RdotE = R[X]*E[X] + R[Y]*E[Y] + R[Z]*E[Z];
		if(RdotE < 0) RdotE = 0;
		if(RdotE > 1) RdotE = 1;

		specular[X] += render->lights[i].color[X] * pow(RdotE, render->spec);
		diffuse[X] += render->lights[i].color[X] * NdotL;

		specular[Y] += render->lights[i].color[Y] * pow(RdotE, render->spec);
		diffuse[Y] += render->lights[i].color[Y] * NdotL;

		specular[Z] += render->lights[i].color[Z] * pow(RdotE, render->spec);
		diffuse[Z] += render->lights[i].color[Z] * NdotL;
	}
	vertexColor->x = render->Ks[X] * specular[X] + render->Kd[X] * diffuse[X] + render->Ka[X] * render->ambientlight.color[X];
	vertexColor->y = render->Ks[Y] * specular[Y] + render->Kd[Y] * diffuse[Y] + render->Ka[Y] * render->ambientlight.color[Y];
	vertexColor->z = render->Ks[Z] * specular[Z] + render->Kd[Z] * diffuse[Z] + render->Ka[Z] * render->ambientlight.color[Z];
}

float areaOfTriangle(Vertex vertex1, Vertex vertex2, Vertex vertex3) {
	return 0.5 * abs(vertex1.x* (vertex2.y-vertex3.y) + vertex2.x*(vertex3.y-vertex1.y) + vertex3.x*(vertex1.y-vertex2.y));
}

void barycentricInterpolation(Vertex *vertices, Vertex *targetVertex, Vertex *vertexValues, Vertex *targetValue) {
	float A1, A2, A3, A;
	A = areaOfTriangle(vertices[0], vertices[1], vertices[2]);
	if (A < 0.000000001) return;
	A1 = areaOfTriangle(vertices[2], vertices[1], *targetVertex);
	A2 = areaOfTriangle(vertices[0], vertices[2], *targetVertex);
	A3 = areaOfTriangle(vertices[0], vertices[1], *targetVertex);
	targetValue->x = A1/A* vertexValues[0].x + A2/A* vertexValues[1].x + A3/A* vertexValues[2].x;
	targetValue->y = A1/A* vertexValues[0].y + A2/A* vertexValues[1].y + A3/A* vertexValues[2].y;
	targetValue->z = A1/A* vertexValues[0].z + A2/A* vertexValues[1].z + A3/A* vertexValues[2].z;
}


int GzPutTriangle(GzRender *render, int numParts, GzToken *nameList, GzPointer *valueList)
/* numParts : how many names and values */
{
	Vertex vertices[3];
	Edge edges[3];

	Vertex normals[3];
	Vertex vertexColors[3];

	if (render == NULL || nameList == NULL || valueList == NULL) return GZ_FAILURE;
	for (int j = 0; j < numParts; j++) {
		if (nameList[j] == GZ_POSITION) {
			GzCoord* coord = (GzCoord*) valueList[j];
			// Parse the vertices
			for (int i = 0; i < 3; i++) {
				vertices[i].x = (*coord)[3*i+0];
				vertices[i].y = (*coord)[3*i+1];
				vertices[i].z = (*coord)[3*i+2];
			}

			for (int i = 0; i < 3; i++) {
				float _x, _y, _z, _w;
				_x = render->Ximage[render->matlevel - 1][0][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][0][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][0][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][0][3];
				_y = render->Ximage[render->matlevel - 1][1][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][1][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][1][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][1][3];
				_z = render->Ximage[render->matlevel - 1][2][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][2][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][2][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][2][3];
				_w = render->Ximage[render->matlevel - 1][3][0] * vertices[i].x +
						render->Ximage[render->matlevel - 1][3][1] * vertices[i].y +
						render->Ximage[render->matlevel - 1][3][2] * vertices[i].z +
						render->Ximage[render->matlevel - 1][3][3];
				vertices[i].x = _x / _w;
				vertices[i].y = _y / _w;
				vertices[i].z = _z / _w;

				if ((vertices[i].x < 0 || vertices[i].x > render->display->xres) &&
					(vertices[i].y < 0 || vertices[i].y > render->display->yres) &&
					(vertices[i].z < 0))
				{
					return GZ_FAILURE;
				}
			}
		} else if (nameList[j] == GZ_NORMAL) {
			GzCoord* coord = (GzCoord*) valueList[j];
			// Parse the normals
			for (int i = 0; i < 3; i++) {
				normals[i].x = (*coord)[3*i+0];
				normals[i].y = (*coord)[3*i+1];
				normals[i].z = (*coord)[3*i+2];
			}
			// Transform the normals from model space to image space
			for (int i = 0; i < 3; i++) {
				float _x, _y, _z, _w;
				_x = render->Xnorm[render->matlevel - 1][0][0] * normals[i].x +
						render->Xnorm[render->matlevel - 1][0][1] * normals[i].y +
						render->Xnorm[render->matlevel - 1][0][2] * normals[i].z +
						render->Xnorm[render->matlevel - 1][0][3];
				_y = render->Xnorm[render->matlevel - 1][1][0] * normals[i].x +
						render->Xnorm[render->matlevel - 1][1][1] * normals[i].y +
						render->Xnorm[render->matlevel - 1][1][2] * normals[i].z +
						render->Xnorm[render->matlevel - 1][1][3];
				_z = render->Xnorm[render->matlevel - 1][2][0] * normals[i].x +
						render->Xnorm[render->matlevel - 1][2][1] * normals[i].y +
						render->Xnorm[render->matlevel - 1][2][2] * normals[i].z +
						render->Xnorm[render->matlevel - 1][2][3];
				_w = render->Xnorm[render->matlevel - 1][3][0] * normals[i].x +
						render->Xnorm[render->matlevel - 1][3][1] * normals[i].y +
						render->Xnorm[render->matlevel - 1][3][2] * normals[i].z +
						render->Xnorm[render->matlevel - 1][3][3];
				normals[i].x = _x/ _w;
				normals[i].y = _y/ _w;
				normals[i].z = _z/ _w;
				// Normalizing the normals
				float mag = sqrt(normals[i].x* normals[i].x + normals[i].y*normals[i].y + normals[i].z*normals[i].z);
				normals[i].x /= mag;
				normals[i].y /= mag;
				normals[i].z /= mag;
			}
		}
	}

	float zA, zB, zC, zD;
	float redA, redB, redC, redD;
	float greenA, greenB, greenC, greenD;
	float blueA, blueB, blueC, blueD;
	float nXA, nXB, nXC, nXD;
	float nYA, nYB, nYC, nYD;
	float nZA, nZB, nZC, nZD;

	if (render->interp_mode == GZ_COLOR) {
		for (int i = 0; i < 3; i++) {
			getColorFromNormal(render, &normals[i], &vertexColors[i]);
		}
		// Sort the vertices in the ascending order of Y coordinate
		sortThreeVerticesInPlace(vertices, vertexColors);

		// Find equation of the plane in which triangle is present.
		// Ax + By + Cz + D = 0;
		findPlaneEquation(vertices, &zA, &zB, &zC, &zD);

		vertices[0].z = vertexColors[0].x; vertices[1].z = vertexColors[1].x; vertices[2].z = vertexColors[2].x;
		findPlaneEquation(vertices, &redA, &redB, &redC, &redD);

		vertices[0].z = vertexColors[0].y; vertices[1].z = vertexColors[1].y; vertices[2].z = vertexColors[2].y;
		findPlaneEquation(vertices, &greenA, &greenB, &greenC, &greenD);

		vertices[0].z = vertexColors[0].z; vertices[1].z = vertexColors[1].z; vertices[2].z = vertexColors[2].z;
		findPlaneEquation(vertices, &blueA, &blueB, &blueC, &blueD);

	} else if (render->interp_mode == GZ_NORMAL) {
		// Sort the vertices in the ascending order of Y coordinate
		sortThreeVerticesInPlace(vertices, normals);

		// Find equation of the plane in which triangle is present.
		// Ax + By + Cz + D = 0;
		findPlaneEquation(vertices, &zA, &zB, &zC, &zD);

		vertices[0].z = normals[0].x; vertices[1].z = normals[1].x; vertices[2].z = normals[2].x;
		findPlaneEquation(vertices, &nXA, &nXB, &nXC, &nXD);

		vertices[0].z = normals[0].y; vertices[1].z = normals[1].y; vertices[2].z = normals[2].y;
		findPlaneEquation(vertices, &nYA, &nYB, &nYC, &nYD);

		vertices[0].z = normals[0].z; vertices[1].z = normals[1].z; vertices[2].z = normals[2].z;
		findPlaneEquation(vertices, &nZA, &nZB, &nZC, &nZD);
	}

	// Create edges such that they are in Clockwise/CounterClockwise direction
	createEdgesInCCWdirection(edges, vertices);

	// Find the bounding box for the triangle
	float minX, minY, maxX, maxY;
	findBoundingBox(&minX, &minY, &maxX, &maxY, vertices);

	// Iterate over all the pixels in the bounding box
	// On every pixel evaluate edge function for all three edges
	// If signs of all the three edge functions are same for a given pixel
	// that pixel is inside the triangle. We should render that pixel
	for (int x = floor(minX); x < ceil(maxX); x++) {
		for (int y = floor(minY); y < ceil(maxY); y++) {
			if ((
				  (edges[0].A * x + edges[0].B * y + edges[0].C) >= 0  &&
				  (edges[1].A * x + edges[1].B * y + edges[1].C) >= 0  &&
				  (edges[2].A * x + edges[2].B * y + edges[2].C) >= 0
				) ||
				(
				  (edges[0].A * x + edges[0].B * y + edges[0].C) <= 0  &&
				  (edges[1].A * x + edges[1].B * y + edges[1].C) <= 0  &&
				  (edges[2].A * x + edges[2].B * y + edges[2].C) <= 0
				)
			)
			{
				GzIntensity r,g,b,a;
				int previousDepth;
				GzGetDisplay(render->display, x, y, &r, &g ,&b, &a, &previousDepth);
				int z = (-1*zD-1*zB*y-1*zA*x)/zC;

				if (previousDepth == 0 || previousDepth > z) {
					Vertex targetColor;
					if (render->interp_mode == GZ_COLOR) {
						Vertex targetVertex; targetVertex.x = x; targetVertex.y = y; targetVertex.z = z;
						barycentricInterpolation(vertices, &targetVertex, vertexColors, &targetColor);
						//targetColor.x = (-1*redD-1*redB*y-1*redA*x)/redC;
						//targetColor.y = (-1*greenD-1*greenB*y-1*greenA*x)/greenC;
						//targetColor.z = (-1*blueD-1*blueB*y-1*blueA*x)/blueC;
					} else if (render->interp_mode == GZ_NORMAL) {
						Vertex normal;
						normal.x = (-1*nXD-1*nXB*y-1*nXA*x)/nXC;
						normal.y = (-1*nYD-1*nYB*y-1*nYA*x)/nYC;
						normal.z = (-1*nZD-1*nZB*y-1*nZA*x)/nZC;
						float mag = sqrt(normal.x*normal.x+normal.y*normal.y+normal.z*normal.z);
						normal.x /= mag; normal.y /= mag; normal.z /= mag;
						getColorFromNormal(render, &normal, &targetColor);
					}
					GzPutDisplay(render->display, x, y, (GzIntensity ) ctoi((float) targetColor.x), (GzIntensity ) ctoi((float) targetColor.y), (GzIntensity ) ctoi((float) targetColor.z), a, z);
				}
			}
		}
	}

	return GZ_SUCCESS;
}

/* NOT part of API - just for general assistance */

short	ctoi(float color)		/* convert float color to GzIntensity short */
{
  return(short)((int)(color * ((1 << 12) - 1)));
}


void findPlaneEquation(Vertex *vertices, float *zA, float *zB, float *zC, float *zD) {
	float x0,y0,z0,x1,y1,z1;
	x0 = vertices[1].x - vertices[0].x;
	x1 = vertices[2].x - vertices[0].x;
	y0 = vertices[1].y - vertices[0].y;
	y1 = vertices[2].y - vertices[0].y;
	z0 = vertices[1].z - vertices[0].z;
	z1 = vertices[2].z - vertices[0].z;

	*zA = y0*z1-z0*y1;
	*zB = x1*z0-x0*z1;
	*zC = x0*y1-y0*x1;
	*zD = -(*zA)*vertices[0].x - (*zB)*vertices[0].y - (*zC)*vertices[0].z;
}

void printVertex(Vertex* v) {
	printf("VERTEX = (%.3f , %.3f, %.3f) \n", v->x, v->y, v->z);
}

void printEdge(Edge* edge) {
	printf("EDGE = (%.3f , %.3f, %.3f) TO (%.3f, %.3f, %.3f) \n", edge->x1, edge->y1, edge->z1, edge->x2, edge->y2, edge->z2);
}


void sortThreeVerticesInPlace(Vertex *vertices, Vertex *colors) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	Vertex* c1 = colors + 0;
	Vertex* c2 = colors + 1;
	Vertex* c3 = colors + 2;

	float y1 = v1->y;
	float y2 = v2->y;
	float y3 = v3->y;
	if (y1 < y2) {
		if (y1 < y3) {
			if (y2 < y3) {
				// 1 < 2 < 3
				return;
			} else {
				// 1 < 3 < 2
				Vertex tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
				Vertex tmpC = *c3;
				*c3 = *c2;
				*c2 = tmpC;
			}
		} else {
			// 3 < 1 < 2
			Vertex tmp = *v3;
			*v3 = *v1;
			*v1 = tmp;
			tmp = *v3;
			*v3 = *v2;
			*v2 = tmp;
			Vertex tmpC = *c3;
			*c3 = *c1;
			*c1 = tmpC;
			tmpC = *c3;
			*c3 = *c2;
			*c2 = tmpC;
		}
	} else {
		if (y2 < y3) {
			if (y3 < y1) {
				// 2 < 3 < 1
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
				tmp = *v3;
				*v3 = *v2;
				*v2 = tmp;
				Vertex tmpC = *c1;
				*c1 = *c2;
				*c2 = tmpC;
				tmpC = *c3;
				*c3 = *c2;
				*c2 = tmpC;
			} else {
				// 2 < 1 < 3
				Vertex tmp = *v1;
				*v1 = *v2;
				*v2 = tmp;
				Vertex tmpC = *c1;
				*c1 = *c2;
				*c2 = tmpC;
			}
		} else {
			// 3 < 2 < 1
			Vertex tmp = *v1;
			*v1 = *v3;
			*v3 = tmp;
			Vertex tmpC = *c1;
			*c1 = *c3;
			*c3 = tmpC;
		}
	}
}

void createEdgesInCCWdirection(Edge* edges, Vertex* vertices) {
	Vertex* v1 = vertices + 0;
	Vertex* v2 = vertices + 1;
	Vertex* v3 = vertices + 2;
	// Find equation Ax + By + C = 0 for edge 1 - 3
    float A = (v3->y - v1->y) / (v3->x - v1->x);
    float C = v1->y - A * v1->x;
    float B = -1;
    // Find the value of x for y=v2->y;
    float x = (-B*v2->y - C)/ A;
    if (x < v2->x) { // It means that v2 is present on the right side
    	// CW direction is (1-2) , (2-3), (3-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v2->x; edges[0].y2 = v2->y; edges[0].z2 = v2->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v2->x; edges[1].y1 = v2->y; edges[1].z1 = v2->z; edges[1].x2 = v3->x; edges[1].y2 = v3->y; edges[1].z2 = v3->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v2->y - (edges[1].y2 - edges[1].y1) * v2->x;
    	edges[2].x1 = v3->x; edges[2].y1 = v3->y; edges[2].z1 = v3->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v3->y - (edges[2].y2 - edges[2].y1) * v3->x;
    } else {
    	// CW direction is (1-3) , (3-2), (2-1)
    	edges[0].x1 = v1->x; edges[0].y1 = v1->y; edges[0].z1 = v1->z; edges[0].x2 = v3->x; edges[0].y2 = v3->y; edges[0].z2 = v3->z;
    	edges[0].A = (edges[0].y2 - edges[0].y1);
    	edges[0].B = -1 * (edges[0].x2 - edges[0].x1);
    	edges[0].C = (edges[0].x2 - edges[0].x1) * v1->y - (edges[0].y2 - edges[0].y1) * v1->x;
    	edges[1].x1 = v3->x; edges[1].y1 = v3->y; edges[1].z1 = v3->z; edges[1].x2 = v2->x; edges[1].y2 = v2->y; edges[1].z2 = v2->z;
    	edges[1].A = (edges[1].y2 - edges[1].y1);
    	edges[1].B = -1 * (edges[1].x2 - edges[1].x1);
    	edges[1].C = (edges[1].x2 - edges[1].x1) * v3->y - (edges[1].y2 - edges[1].y1) * v3->x;
    	edges[2].x1 = v2->x; edges[2].y1 = v2->y; edges[2].z1 = v2->z; edges[2].x2 = v1->x; edges[2].y2 = v1->y; edges[2].z2 = v1->z;
    	edges[2].A = (edges[2].y2 - edges[2].y1);
    	edges[2].B = -1 * (edges[2].x2 - edges[2].x1);
    	edges[2].C = (edges[2].x2 - edges[2].x1) * v2->y - (edges[2].y2 - edges[2].y1) * v2->x;
    }
}

void findBoundingBox(float* minX, float* minY, float* maxX, float*maxY, Vertex* vertices) {
	*minX = vertices[0].x;
	*minY = vertices[0].y;
	*maxX = vertices[0].x;
	*maxY = vertices[0].y;
	for (int i = 0; i < 3; i++) {
		if (vertices[i].x < *minX) *minX = vertices[i].x;
		if (vertices[i].y < *minY) *minY = vertices[i].y;
		if (vertices[i].x > *maxX) *maxX = vertices[i].x;
		if (vertices[i].y > *maxY) *maxY = vertices[i].y;
	}
}

