Name: Amey Vaidya
CSCI 580, HW6, USCID: 2836139499
Email: ameynanv@usc.edu
===================================
Default output of this program will render image similar to "Smooth.ppm"
It will render image of only pot, without any texture, while using Phong shading
   
1) To generate image similar to "ppot.p.tex.aa.ppm" please follow following steps:
   a) Open Application6.cpp
   b) Goto line number 10-11. Change the INFILE from "pot4.ppm" to "ppot.ppm"
   c) Goto line number 111. Enable the app defined camera configuration
   d) Goto line number 176. Enable texturing by setting the "if" statement to "1"
   e) Make and run the program.

2) To generate image similar to "Smooth.ppm" please follow following steps:
   a) Open Application6.cpp
   b) Goto line number 10-11. Change the INFILE from "ppot.ppm" to "pot4.ppm"
   c) Goto line number 111. Disable the app defined camera configuration by setting the "if" statement to "0"
   d) Goto line number 176. Disable texturing by setting the "if" statement to "0"
   e) Make and run the program.
   
3) To use Gouraud shading instead of Phong
   a) Open Application6.cpp
   c) Goto line number 161. Enable Gourand Shading by setting the "if" statement to "1"
   e) Make and run the program.

4) To use Phong shading instead of Gourand
   a) Open Application6.cpp
   c) Goto line number 161. Enable Phong Shading by setting the "if" statement to "0"
   e) Make and run the program.
   