// Application6.h: interface for the Application6 class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#define	AAKERNEL_SIZE	6

struct GzDisplay;
struct GzRender;

class Application6
{
	public:
		int runRenderer();
	protected:
		GzDisplay* display[AAKERNEL_SIZE]; // the display
		GzRender* render[AAKERNEL_SIZE];   // the renderer
};
